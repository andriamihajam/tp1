package fr.umontpellier.iut.exercice2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.util.Random;

public class TicTacToe extends Application {

    @Override
    public void start(Stage primaryStage) {
        GridPane g=new GridPane();
        Scene scene=new Scene(g,g.getMinWidth(),g.getMinHeight());
        primaryStage.setResizable(false);

        Label croix =new Label("croix");
        Label rond =new Label("rond");
        Label vide=new Label("vide");


        for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
                Random random = new Random();
                int nombre = random.nextInt(3);
                if (nombre ==  0){
                    ImageView iv1=new ImageView("exercice2/Croix.png");
                    // croix.setGraphic(iv1);
                    g.addRow(i,iv1);
                }
                else if(nombre ==1) {
                    ImageView iv2=new ImageView("exercice2/Rond.png");
                    //rond.setGraphic(iv2);
                    g.addRow(i,iv2);
                }
                else {
                    ImageView iv3=new ImageView("exercice2/Vide.png");
                    //vide.setGraphic(iv3);
                    g.addRow(i,iv3);
                }
            }
        }
        //g.setVgap(5);
        // g.setHgap(5);
        //g.setGridLinesVisible(true);

        primaryStage.setTitle("Tic Tac Toe");
        primaryStage.show();
        primaryStage.setScene(scene);
    }
}


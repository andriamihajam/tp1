package fr.umontpellier.iut.exercice3;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;


public class MaPremiereFenetreJavaFX extends Application {

    public static void main(String[] args) {
        launch(args);
    }
    private Label labelBonjour;

    private TextField nom;

    // private final EventHandler<ActionEvent> actionClavier= keyEvent -> System.out.println("Bonjour"+ nom.getText());

    private final EventHandler<ActionEvent> actionBouton=  event ->labelBonjour.setText("Bonjour"+ nom.getText());

    // Créer une classe anonyme
    EventHandler<MouseEvent> action2= new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent actionMouse) {
            System.out.println("Bonjour via la classe");
        }
    };
    // EventHandler<KeyEvent> action3=new EventHandler<KeyEvent>() {
    //  @Override
    //  public void handle(KeyEvent keyEvent) {
    //    System.out.println("Bonjour via la classe");
    //   }
    // };
    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox root =new VBox();
        root.setAlignment(Pos.CENTER);
        labelBonjour=new Label("Bonjour à tous !");
        root.getChildren().add(labelBonjour);
        nom =new TextField();
        nom.setMaxWidth(150);
        root.getChildren().add(nom);
        Button direBonjour= new Button();
        root.getChildren().add(direBonjour);
        // direBonjour.setOnAction(event -> System.out.println("Bonjour"+" "+ nom.getText()));
//        direBonjour.addEventHandler(MouseEvent.MOUSE_CLICKED,event -> System.out.println("Bonjour"+""+ nom.getText()));
        direBonjour.setOnAction(actionBouton);
        Scene scene = new Scene(root);


        ImageView imageView=new ImageView("https://gitlabinfo.iutmontp.univ-montp2.fr/ihm/tp1/-/raw/master/images/logo.jpeg");
        direBonjour.setGraphic(imageView);
        root.getChildren().add(imageView);
        nom.setFont(Font.font("Courier", FontWeight.NORMAL,15));
        labelBonjour.setFont(Font.font("Bold",FontWeight.NORMAL,30));
        nom.setOnAction(actionBouton);


        primaryStage.setTitle("Hello Application");
        primaryStage.setWidth(400);
        primaryStage.setHeight(400);
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}

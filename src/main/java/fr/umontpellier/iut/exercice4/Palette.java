package fr.umontpellier.iut.exercice4;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Palette extends Application {

    private int nbVert = 0;
    private int nbRouge = 0;
    private int nbBleu = 0;

    private Button vert;
    private Button rouge;
    private Button bleu;

    private BorderPane root;
    private Label label;
    private Pane panneau;
    private HBox bas;


    @Override
    public void start(Stage primaryStage) throws Exception {
        root=new BorderPane();
        vert=new Button("Vert");
        rouge=new Button("Rouge");
        bleu=new Button("Bleu");
        bas=new HBox();
        label=new Label();
        panneau=new Pane();

        Scene scene = new Scene(root,400,200);

        vert.setOnAction(event-> {
            nbVert++;
            label.setText("vert choisi "+nbVert+"fois");
            panneau.setStyle("-fx-background-color: green");
        } );

        rouge.setOnAction(event -> {
            nbRouge++;
            label.setText("rouge choisi"+ nbRouge+"fois");
            panneau.setStyle("-fx-background-color: red");
        });
        bleu.setOnAction(event -> {
            label.setText("bleu choisi"+nbBleu+"fois");
            panneau.setStyle("-fx-background-color: blue");
        });
        bas.getChildren().addAll(vert,rouge,bleu);
        root.setBottom(bas);
        root.setTop(label);
        root.setCenter(panneau);
        bas.setAlignment(Pos.CENTER);
        bas.setSpacing(10);




        primaryStage.setScene(scene);
        primaryStage.show();
    }
}


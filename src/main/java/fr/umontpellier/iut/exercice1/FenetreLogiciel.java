package fr.umontpellier.iut.exercice1;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class FenetreLogiciel extends Application {

    @Override
    public void start(Stage primaryStage) {
        BorderPane root = new BorderPane();


        Menu menu1=new Menu("File");
        Menu menu2= new Menu("Edit");
        Menu menu3 = new Menu("Help");

        MenuItem menuItem =new MenuItem("New");
        MenuItem menuItem1= new MenuItem("Open");
        MenuItem menuItem2=new MenuItem("Edit");
        MenuItem menuItem3=new MenuItem("Close");

        MenuItem menuItem4 = new MenuItem("Cut");
        MenuItem menuItem5 = new MenuItem("Copy");
        MenuItem menuItem6= new MenuItem("Paste");

        menu2.getItems().addAll(menuItem4,menuItem5,menuItem6);
        menu1.getItems().addAll(menuItem,menuItem1,menuItem2,menuItem3);

        MenuBar menuBar = new MenuBar(menu1, menu2, menu3);
        root.setTop(menuBar);

        Scene scene = new Scene(root,600,400);

        VBox vBox = new VBox();
        Button button1=new Button("Bouton 1");
        Button button2= new Button("Bouton 2");
        Button button3 = new Button("Bouton 3");

        Label label = new Label("Boutons :");

        vBox.setSpacing(10);
        vBox.getChildren().addAll(label);
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().addAll(button1,button2,button3);
        root.setLeft(vBox);


        VBox vBox1 =new VBox();
        GridPane gridPane =new GridPane();

        Label name = new Label("Name :");
        gridPane.add(name,0,0);
        TextField champ1= new TextField();
        gridPane.add(champ1,1,0);

        Label name2= new Label("Email :");
        gridPane.add(name2,0,1);
        TextField champ2=new TextField();
        gridPane.add(champ2,1,1);

        Label name3= new Label("Password :");
        gridPane.add(name3,0,2);
        TextField champ3= new TextField();
        gridPane.add(champ3,1,2);



        root.setCenter(gridPane);
        gridPane.setAlignment(Pos.CENTER);

        HBox hBox = new HBox();
        Button s =new Button("Submit");
        Button c =new Button("Cancel");
        hBox.getChildren().addAll(c,s);
        hBox.setAlignment(Pos.CENTER);
        vBox.getChildren().addAll(hBox);
        //root.setCenter(vBox);
        vBox1.setAlignment(Pos.CENTER);

        gridPane.setHgap(25);
        gridPane.setHgap(10);

        Label bas= new Label("Ceci est un label de bas de page");
        root.setBottom(bas);
        BorderPane.setAlignment(bas,Pos.CENTER);
        vBox1.getChildren().addAll(gridPane,hBox);
        root.setCenter(vBox1);
        primaryStage.setTitle("Premier exemple manipulant les conteneurs");
        primaryStage.show();
        primaryStage.setScene(scene);
    }

    public static void main(String[] args) {
        launch(args);

    }
}

